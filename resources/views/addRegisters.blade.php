@extends('layouts.app')

@section('content')
<script>var num = 1;
        var counter = 2 </script>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div  style="float: right; padding: 10px">
                   <div class="btn btn-primary" id="add">add more row</div>
                </div>

                <div class="panel-body">
                    <form role="form" method="POST" action="{{route('addRegisterSubmit')}}">
                        {{ csrf_field() }}

                        <table class = "table table-striped" id="add_container">

                            <thead>
                            <tr>
                                <th>No</th>
                                <th>name</th>
                                <th>ip</th>
                                <th>deviceID</th>
                                <th>register</th>
                                <th>size</th>
                                <th>type</th>
                                <th>delete</th>

                            </tr>
                            </thead>

                            <tbody>
                            <tr id="row_1">
                                <td>1</td>
                                <td><input id="name_1" type="text" name="name[]" class="form-control" required></td>
                                <td><input id="ip_1" type="text" name="ip[]" class="form-control" required></td>
                                <td><input id="deviceID_1" type="text" name="deviceID[]" class="form-control" required></td>
                                <td><input id="register_1" type="text" name="register[]" class="form-control" required></td>
                                <td><input id="size_1" type="text" name="size[]" class="form-control" required></td>
                                <td><input id="type_1" type="text" name="type[]" class="form-control" required></td>
                                <td><a class="btn btn-danger rem" id="remove_1">remove</a></td>
                            </tr>

                            </tbody>

                        </table>
                            <div>
                                <button type="submit" class="btn btn-default">
                                    <i>submit</i>
                                </button>
                            </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){

        $("#add").click(function () {
            if(num<10){
                num++;
                console.log('added');
                $("#add_container").append('<tr id="row_'+counter+'"> <td>'+counter+'</td> <td><input id="name_'+counter+'" type="text" name="name[]" class="form-control" required></td> <td><input id="ip_'+counter+'" type="text" name="ip[]" class="form-control" required></td> <td><input id="deviceID_'+counter+'" type="text" name="deviceID[]" class="form-control" required></td> <td><input id="register_'+counter+'" type="text" name="register[]" class="form-control" required></td> <td><input id="size_'+counter+'" type="text" name="size[]" class="form-control" required></td> <td><input id="type_'+counter+'" type="text" name="type[]" class="form-control" required></td> <td><a id="remove_'+counter+'" class="btn btn-danger rem">remove</a></td> </tr>');
                counter++;


            }else {
                console.log('not added');

            }
        });

        $(document).on('click', '.rem', function(){
            console.log('remove was clicked');
            var myId = $(this).attr('id').toString();
            var valu = myId.replace("remove_","");
            var containID = "#row_"+valu;
            console.log(containID);
            $(containID).remove();
            num--;
        });

//        $(".rem").click(function () {
//            console.log('remove was clicked');
//            var myId = $(this).attr('id').toString();
//            var valu = myId.replace("remove_","");
//            var containID = "#row_"+valu;
//            console.log(containID);
//            $(containID).remove();
//            num--;
//        });

    });
</script>
@endsection
