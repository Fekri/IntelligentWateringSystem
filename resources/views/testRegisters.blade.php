@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <table class = "table table-striped">
                        <caption>Register Test</caption>

                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>name</th>
                            <th>ip</th>
                            <th>deviceID</th>
                            <th>register</th>
                            <th>size</th>
                            <th>type</th>
                            <th>status</th>
                            <th>test</th>

                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $counter =1;
                            $i = 0;
                        @endphp
                        @if(count($register))
                            @foreach($register as $r)
                                <tr>
                                    <td>{{$counter}}</td>
                                    <td>{{$r->name}}</td>
                                    <td>{{$r->ip}}</td>
                                    <td>{{$r->deviceID}}</td>
                                    <td>{{$r->register}}</td>
                                    <td>{{$r->size}}</td>
                                    <td>{{$r->type}}</td>
                                    <td>{{$r->status}}</td>
                                    <td>{{$value[$i]}}</td>
                                </tr>
                                @php
                                    $counter =1+$counter;
                                    $i++;
                                @endphp
                            @endforeach
                        @endif
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
