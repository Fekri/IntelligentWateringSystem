@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <table class = "table table-striped">
                        <caption>The excel you have imported, contains data as following below:</caption>

                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>name</th>
                            <th>ip</th>
                            <th>deviceID</th>
                            <th>register</th>
                            <th>size</th>
                            <th>type</th>

                        </tr>
                        </thead>

                        <tbody>
                        @php
                            $counter =1;
                        @endphp
                        @if(count($register))
                            @foreach($register as $r)
                                <tr>
                                    <td>{{$counter}}</td>
                                    <td>{{$r->name}}</td>
                                    <td>{{$r->ip}}</td>
                                    <td>{{$r->device}}</td>
                                    <td>{{$r->register}}</td>
                                    <td>{{$r->size}}</td>
                                    <td>{{$r->type}}</td>
                                </tr>
                                @php
                                    $counter =1+$counter;
                                @endphp
                            @endforeach
                        @endif
                        </tbody>

                    </table>
                </div>
                @if(count($register))
                    <center><a class="btn btn-primary" href="{{route('excelsubmit')}}"> confirm </a></center>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
