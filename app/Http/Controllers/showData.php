<?php

namespace App\Http\Controllers;




use App\Classes\ModBusFetcher;
use App\Classes\ModBusFetcherAPI;
use App\Classes\ModBusWriterAPI;
use Illuminate\Http\Request;

class showData extends Controller
{
    function index(){
        $md = new ModBusFetcher();
        $data = $md->index();
        return view('welcome',['data'=>$data]);
    }

    function api(Request $request, $ip, $id, $register, $size, $type){

        $md = new ModBusFetcherAPI('noname',$id,$ip,$size,$register,$type);
        return $md->index();
    }

    function auto(Request $request, $ip, $id, $register, $size, $type){
        $md = new ModBusFetcherAPI('noname',$id,$ip,$size,$register,$type);
        $md->autoSchedulSaver();
        echo 'ok';
    }

    function writer(Request $request, $ip, $id, $register, $type, $data){
        $md = new ModBusWriterAPI($id,$ip,$register,$type,$data);
        $md->index();
    }
}
