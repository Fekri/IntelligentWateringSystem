<?php

namespace App\Http\Controllers;

use App\Classes\ModBusFetcherAPI;
use App\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Excel;

class Registers extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index(){
        return view('home');
    }

    public function addRegisterShow(Request $request){
        return view('addRegisters');
    }

    public function addRegisterSubmit(Request $request){
        for($i = 0 ; $i < count($request->name) ; $i++){
            $reg = new Register();
            $reg->name = $request->name[$i];
            $reg->ip = $request->ip[$i];
            $reg->deviceID = $request->deviceID[$i];
            $reg->register = $request->register[$i];
            $reg->size = $request->size[$i];
            $reg->type = $request->type[$i];
            $reg->save();
        }
        return redirect()->route('registerShowAll');
    }

    public function registerShowAll(Request $request){
        $r = Register::orderBy('created_at', 'desc')->paginate(40);
        return view('Registers',['register'=>$r]);
    }

    public function registerEditShow(Register $register){
        return view('editRegisters',['r'=>$register]);
    }

    public function registerEditSubmit(Request $request, Register $register){
        $register->name = $request->name;
        $register->ip = $request->ip;
        $register->deviceID = $request->deviceID;
        $register->register = $request->register;
        $register->size = $request->size;
        $register->type = $request->type;
        $register->status = $request->status;
        $register->save();
        return redirect()->route('registerShowAll');
    }

    public function registerDelete(Request $request, Register $register){
        $register->delete();
        return redirect()->route('registerShowAll');
    }


    public function registerTest(){

        $value = [];
        $reg = Register::all();
        $i = 0;

        foreach ($reg as $r){
            $md = new ModBusFetcherAPI($r->name,$r->deviceID,$r->ip,$r->size,$r->register,$r->type);
            $value[$i] = $md->index();
            $i++;
        }

        return view('testRegisters',['register'=>$reg,
                                            'value'=>$value]);
    }

    public function importExcelView(){
        return view('excelupload');
    }

    public function importExcel(Request $request){
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $ext = Input::file('import_file')->getClientOriginalExtension();

            if($ext == 'xls' || $ext == 'xlsx'){
                $data = \Maatwebsite\Excel\Facades\Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){
//                    foreach ($data as $value) {
//                        //$insert[] = ['title' => $value->title, 'description' => $value->description];
//                        echo $value;
//                    }
                    $request->session()->put('register',$data);
                    return view('RegisterExcelAck',['register'=>$data]);
                }
            }else{
                echo 'not valid extension';
            }

        }
    }

    public function importedExcelSubmit(Request $request){
        $data = $request->session()->get('register');

        foreach ($data as $value){
            $reg = new Register();
            $reg->name = $value->name;
            $reg->ip = $value->ip;
            $reg->deviceID = $value->device;
            $reg->register = $value->register;
            $reg->size = $value->size;
            $reg->type = $value->type;
            $reg->save();
        }
        return redirect()->route('registerShowAll');
    }
}
