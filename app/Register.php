<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public $timestamps = true;
    protected $table = 'register';
    protected $primaryKey = 'id';
}
