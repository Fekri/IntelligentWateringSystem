<?php

namespace App\Console\Commands;

use App\Classes\ModBusFetcherAPI;
use App\Register;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class dataFetch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will fetch data from modbus webservice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reg = Register::all();
        foreach ($reg as $r){
            $md = new ModBusFetcherAPI($r->name,$r->deviceID,$r->ip,$r->size,$r->register,$r->type);
            $md->autoSchedulSaver();
            echo 'data was fetched';
            Log::warning("data was fetched");
        }

    }
}
