<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModBusReg extends Model
{
    public $timestamps = true;
    protected $table = 'modbus';
    protected $primaryKey = 'id';
}
