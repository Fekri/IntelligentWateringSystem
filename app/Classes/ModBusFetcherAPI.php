<?php
/**
 * Created by PhpStorm.
 * User: Pedram
 * Date: 12/16/2017
 * Time: 12:47 PM
 */
namespace App\Classes;

use App\ModBusReg;
use App\RegisterValue;

include(app_path() . '/Classes/ModBusMaster.php');

class ModBusFetcherAPI
{
    var $name;
    var $id;
    var $ip;
    var $size;
    var $register;
    var $type;
    function __construct($inname,$inid,$inip,$insize,$register,$type)
    {
        $this->id = $inid;
        $this->ip = $inip;
        $this->size = $insize;
        $this->register = $register;
        $this->type = $type;
        $this->name = $inname;

    }

    //185.64.178.109:502
    public function index(){
        $mb = new \ModbusMaster($this->ip,'TCP');


        if($this->type == 'boolean'){

            try {
                // FC 3
                // read 10 words (20 bytes) from device ID=0, address=12288
                $recData = $mb->readCoils($this->id, $this->register, $this->size); //4096 =>9595 float 32bit 4106
            }
            catch (\Exception $e) {
                // Print error information if any
//            echo $mb;
//            echo $e;
                return $e->getMessage();
            }

            return $recData;
        }else{
            try {
                // FC 3
                // read 10 words (20 bytes) from device ID=0, address=12288
                $recData = $mb->readMultipleRegisters($this->id, $this->register , $this->size); //4096 =>9595 float 32bit 4106
            }
            catch (\Exception $e) {
                // Print error information if any
//            echo $mb;
//            echo $e;
                return $e->getMessage();
            }

            // Received data
            //return $recData[1];
            //echo "<h1>Received Data</h1>\n";
            $values = array_chunk($recData, 4);
            //echo $values[0];
//        dd($values);
            return round($this->converter($values, $this->type),2);
            //return \PhpType::bytes2float($values[0]);
//        foreach($values as $bytes)
//            echo \PhpType::bytes2float($bytes) . "</br>";
//        return 5;
        }






    }

    function autoSchedulSaver(){
        $mb = new \ModbusMaster($this->ip,'TCP');

        try {
            // FC 3
            // read 10 words (20 bytes) from device ID=0, address=12288
            $recData = $mb->readMultipleRegisters($this->id, $this->register , $this->size); //4096 =>9595 float 32bit 4106
            $result = 1;
        }
        catch (\Exception $e) {
            // Print error information if any
//            echo $mb;
//            echo $e;
            $result = 0;
            $recData = $e->getMessage();
        }

        if($result == 1){
            $values = array_chunk($recData, 4);
            $data = round($this->converter($values, $this->type),2);

            $modbusreg = new RegisterValue();
            $modbusreg->value = (string) $data;
            $modbusreg->ip = $this->ip;
            $modbusreg->name = $this->name;
            $modbusreg->deviceID = $this->id;
            $modbusreg->register = $this->register;
            $modbusreg->size = $this->size;
            $modbusreg->type = $this->type;
            $modbusreg->save();
        }else{
            $data = $recData;
            $modbusreg = new RegisterValue();
            $modbusreg->value = (string) $data;
            $modbusreg->ip = $this->ip;
            $modbusreg->name = $this->name;
            $modbusreg->deviceID = $this->id;
            $modbusreg->register = $this->register;
            $modbusreg->size = $this->size;
            $modbusreg->type = $this->type;
            $modbusreg->save();
        }
        return 0;

    }

    function converter($input, $type){
        switch ($type){
            case 'boolean':
                return \PhpType::bytes2string($input[0]);
            case 'float':
                return \PhpType::bytes2float($input[0]);
            case 'int':
                return \PhpType::bytes2unsignedInt($input[0]);
            default:
                return \PhpType::bytes2float($input[0]);
        }
    }
}