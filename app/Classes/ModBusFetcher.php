<?php
/**
 * Created by PhpStorm.
 * User: Pedram
 * Date: 12/14/2017
 * Time: 12:04 PM
 */

namespace App\Classes;

use Mockery\Exception;

include(app_path() . '/Classes/ModBusMaster.php');


$id = 0;
class ModBusFetcher
{

    //185.64.178.109:502
    public function index(){
        $mb = new \ModbusMaster('185.64.178.109','TCP');

        try {
            // FC 3
            // read 10 words (20 bytes) from device ID=0, address=12288
            $recData = $mb->readMultipleRegisters(1, 4106 , 20); //4096 =>9595 float 32bit 4106
        }
        catch (\Exception $e) {
            // Print error information if any
//            echo $mb;
//            echo $e;
            return $e->getMessage();
        }


// Received data
        //return $recData[1];
        //echo "<h1>Received Data</h1>\n";
        $values = array_chunk($recData, 4);
        //echo $values[0];
        return \PhpType::bytes2float($values[0]);
//        foreach($values as $bytes)
//            echo \PhpType::bytes2float($bytes) . "</br>";
//        return 5;

    }


}