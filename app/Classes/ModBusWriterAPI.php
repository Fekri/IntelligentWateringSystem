<?php
/**
 * Created by PhpStorm.
 * User: Pedram
 * Date: 12/16/2017
 * Time: 12:47 PM
 */
namespace App\Classes;

use App\ModBusReg;
use App\RegisterValue;
use Mockery\Exception;

include(app_path() . '/Classes/ModBusMaster.php');

class ModBusWriterAPI
{
    var $id;
    var $ip;
    var $data;
    var $register;
    var $type;
    function __construct($inid,$inip,$register,$type,$data)
    {
        $this->id = $inid;
        $this->ip = $inip;
        $this->register = $register;
        $this->type = $type;
        $this->data = $data;
    }

    //185.64.178.109:502
    public function index(){
        // Create Modbus object
        $modbus = new \ModbusMaster($this->ip, "TCP");
        // Data to be writen
        //$f = floatval($this->data);
        //dd($f);
        switch ($this->type){
            case 'float':
                $data = array(floatval($this->data));//array(-1000);
                //$dataTypes = array((string)$this->type);
                $dataTypes = array("REAL");
                try {
                    // FC6
                    $modbus->writeMultipleRegister($this->id, $this->register, $data, $dataTypes);
                }
                catch (Exception $e) {
                    // Print error information if any
                    echo $modbus;
                    echo $e;
                    exit;
                    break;
                }
                // Print status information
                echo $modbus;
                break;
            case 'int':
                $data = array((int)$this->data);//array(-1000);
                //echo $this->data;
                //dd($data);
                //$dataTypes = array();
                $dataTypes = array("INT");
                try {
                    // FC6
                    $modbus->writeMultipleRegister($this->id, $this->register, $data, $dataTypes);
                }
                catch (Exception $e) {
                    // Print error information if any
                    echo $modbus;
                    echo $e;
                    exit;
                    break;
                }
                // Print status information
                echo $modbus;
                break;

            case 'boolean':

                if($this->data == 'true'){
                    $data = array(TRUE);
                }else{
                    $data = array(FALSE);
                }
                try {
                    // FC6
                    $modbus->writeSingleCoil($this->id,$this->register,$data);
                }
                catch (Exception $e) {
                    // Print error information if any
                    echo $modbus;
                    echo $e;
                    exit;
                    break;
                }
                // Print status information
                echo $modbus;
                break;
        }


    }


}