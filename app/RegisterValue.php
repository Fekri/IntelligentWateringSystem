<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterValue extends Model
{
    public $timestamps = true;
    protected $table = 'registervalues';
    protected $primaryKey = 'id';
}
