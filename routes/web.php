<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('modbus')->group(function () {
    Route::get('/fetch', 'showData@index')->name('fetch');
    Route::get('/api/{ip}/{id}/{register}/{size}/{type}','showData@api')->name('api');
    Route::get('/auto/{ip}/{id}/{register}/{size}/{type}','showData@auto')->name('auto');
    Route::get('/writer/{ip}/{id}/{register}/{type}/{data}','showData@writer')->name('writer');
});

Route::prefix('dashboard')->group(function () {
    Route::get('/', 'Registers@index')->name('home');
    Route::get('/addRegisterShow', 'Registers@addRegisterShow')->name('addRegisterShow');
    Route::post('/addRegisterSubmit', 'Registers@addRegisterSubmit')->name('addRegisterSubmit');
    Route::get('/registerShowAll', 'Registers@registerShowAll')->name('registerShowAll');
    Route::get('/register/page/edit/{reg}', 'Registers@registerEditShow')->name('editshow');
    Route::post('/register/edit/{reg}', 'Registers@registerEditSubmit')->name('editSubmit');
    Route::get('/register/delete/{reg}', 'Registers@registerDelete')->name('delete');
    Route::get('/register/test', 'Registers@registerTest')->name('test');
    Route::get('/importExcel/', 'Registers@importExcelView')->name('excelshow');
    Route::post('/importExcel/', 'Registers@importExcel')->name('importExcel');
    Route::get('/importExcel/submit', 'Registers@importedExcelSubmit')->name('excelsubmit');

});

Auth::routes();


//$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
//$this->post('login', 'Auth\LoginController@login');
//$this->post('logout', 'Auth\LoginController@logout')->name('logout');
//
//// Registration Routes...
//$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//$this->post('register', 'Auth\RegisterController@register');
//
//// Password Reset Routes...
//$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
//$this->post('password/reset', 'Auth\ResetPasswordController@reset');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
